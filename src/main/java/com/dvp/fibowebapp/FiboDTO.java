package com.dvp.fibowebapp;


public class FiboDTO {

	private Integer rang;
	private Long valeurExacte;
	private Long valeurApproximative;
	private Long approximation;

	public FiboDTO(Integer rang, Long valeurExacte, Long valeurApproximative) {
		super();
		this.rang = rang;
		this.valeurExacte = valeurExacte;
		this.valeurApproximative = valeurApproximative;
		this.approximation = Math.abs(valeurExacte - valeurApproximative);
	}

	public Integer getRang() {
		return rang;
	}

	public Long getValeurExacte() {
		return valeurExacte;
	}

	public Long getValeurApproximative() {
		return valeurApproximative;
	}
	
	public Long getApproximation() {
		return approximation;
	}

}
