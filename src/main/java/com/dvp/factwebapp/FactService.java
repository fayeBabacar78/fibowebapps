/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dvp.factwebapp;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author faye
 */
public class FactService {
    
    private final static double TwoTimesPi = 2 * Math.PI;
    private static Map<Integer, Long> mapApproximation;
    
    private static FactService instance;
    private static Map<Integer, Long> mapExact;

    private FactService() {
        mapExact = new HashMap<Integer, Long>();
        mapApproximation = new HashMap<Integer, Long>();
    }

    public static synchronized FactService getInstance() {
        if (instance == null) {
            instance = new FactService();
        }
        return instance;
    }

    /**
     *
     * @param number
     * @return factorial of number
     */
    public Long calculateFactOfNumber(int number) {
        if (number <= 0) {
            throw new IllegalArgumentException("La valeur renseignée doit être positive");
        }
        if (number == 1) {
            return 1L;
        }
        Long value = mapExact.get(number);
        if (value != null) {
            return value;
        }
        value = number * calculateFactOfNumber(number - 1);
        mapExact.put(number, value);

        return value;
    }
    
    /**
     *
     * @param number
     * @return approximation of factorial of number
     */
    public Long calculateFactOfNumberApproximatively(int number) {
        if (number <= 0) {
            throw new IllegalArgumentException("La valeur renseignée doit être positive");
        }
        if (number == 1) {
            return 1L;
        }
        final double racine = Math.sqrt(number * TwoTimesPi);
        final double result = racine * Math.pow(number / Math.E, number);
        Long value = Math.round(result);
	mapApproximation.put(number, value);
    
        return value;
    }

}
