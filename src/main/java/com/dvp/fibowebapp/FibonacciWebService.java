package com.dvp.fibowebapp;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public class FibonacciWebService {

	private final FiboService fiboService = FiboService.getInstance();

	public Long fibonacci(@WebParam(name = "rang") Integer rang) {
		return fiboService.calculerFibonacci(rang);
	}

//	public FiboDTO[] fibonacciList(@WebParam(name = "rang") Integer rang, @WebParam(name = "limit") Integer nb) {
//		FiboDTO[] list = new FiboDTO[nb];
//		for (int i = 0; i < nb; i++) {
//			Integer rank = rang - i;
//			Long valeurExacte = fiboService.calculerFibonacci(rank);
//			Long valeurApproximative = fiboService.calculerApproximationfibonacci(rank);
//			list[i] = new FiboDTO(rank, valeurExacte, valeurApproximative);
//		}
//
//		return list;
//	}

}
