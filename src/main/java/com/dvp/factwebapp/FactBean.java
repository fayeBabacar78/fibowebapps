/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dvp.factwebapp;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author faye
 */
@ManagedBean(name = "fact")
public class FactBean {

	private static final Integer DEFAULT_RANK = 7;
	private Integer rank;
	private Long value;
	private List<FactResult> valueSet;
	private final FactService factService = FactService.getInstance();

	private FactResult calculateFactorial(int rank) {
		final Long factorial = getExactFactorial(rank);
		final Long approximativeValue = factService.calculateFactOfNumberApproximatively(rank);
		final FactResult result = new FactResult(rank, factorial, approximativeValue);
		return result;
	}

	public void calculer() {
		valueSet = new ArrayList<FactResult>(rank);
		for (int index = 1; index <= rank; index++) {
			valueSet.add(calculateFactorial(index));
		}
		value = getExactFactorial(rank);
	}

	private Long getExactFactorial(int rank) {
		return factService.calculateFactOfNumber(rank);
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Long getValue() {
		return value;
	}

	public List<FactResult> getValueSet() {
		return valueSet;
	}

	public Integer getDefaultRank() {
		return DEFAULT_RANK;
	}

	public List<FactResult> getReverseValues() {

		final List<FactResult> result = new ArrayList<FactResult>();

		for (int i = valueSet.size() - 1; 0 <= i; i--) {
			result.add(valueSet.get(i));
		}

		return result;
	}

}
