/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dvp.factwebapp;

/**
 *
 * @author faye
 */
public class FactResult {

    private Integer rank;
    private Long factorial;
    private Long approximativeValue;
    private Long approximation;

    FactResult(Integer rank, Long factorial, Long approximativeValue) {
        this.rank = rank;
        this.factorial = factorial;
        this.approximativeValue = approximativeValue;
        this.approximation = Math.abs(factorial - approximativeValue);
    }

    /* Getter et setter */
    public Integer getRank() {
        return rank;
    }

    public Long getFactorial() {
        return factorial;
    }

    public Long getApproximativeValue() {
        return approximativeValue;
    }

    public Long getApproximation() {
        return approximation;
    }

    @Override
    public String toString() {
        return "FactResult [Rang = " + rank + ", Valeur exacte = " + factorial + ","
                + "Valeur approximative = " + approximativeValue + ", Approximation = " + approximation + "]";
    }
}
